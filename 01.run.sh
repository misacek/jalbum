#!/bin/bash

function log(){
    local MY_NAME=${MY_NAME:-$0}
    logger -t ${MY_NAME} -- $@
    echo "$(date) ${MY_NAME}: $@"
}

function trap_exit(){
    log "Trapped error on line $(caller)"
    log "result: failure"
    exit 1
}

function shutdown_x {
    MY_NAME='shutdown_x'
    log "SIGTERM or SIGINT caugt, killing $NODE_PID..."
    kill -s SIGTERM $NODE_PID
    wait $NODE_PID
}

function remote {
    trap shutdown_x SIGTERM SIGINT
    trap 'exit 1' ERR

    MY_NAME='remote'

    Xvfb $CONTAINER_DISPLAY -screen 0 $GEOMETRY &
    export NODE_PID=$!

    for i in $(seq 1 10)
    do
        xdpyinfo -display $CONTAINER_DISPLAY >/dev/null 2>&1
        if [ $? -eq 0 ]; then
            echo "vnc server started"
            break
        fi
        echo Waiting xvfb...
        sleep 0.5
    done

    fluxbox -display $CONTAINER_DISPLAY &

    #x11vnc -ncache -forever -bg -nopw -xkb -usepw -shared -rfbport 5900 -display $DISPLAY

    # create menu entry for jalbum and xterm
    mkdir -p ~/.fluxbox
    cat << EOT > ~/.fluxbox/menu
    [begin]
    [exec] (term) {xvt -e /bin/bash}
    [exec] (jalbum 13.10) {java -jar /jAlbum/JAlbum.jar}
    [reconfig] (Reload config)
    [exit] (Exit)
    [end]
EOT
    cat ~/.fluxbox/menu

    [ -f "${CONTAINER_SSL_CERTIFICATE}" ]
    [ -f "${CONTAINER_SSL_PRIVATE_KEY}" ]
    cat ${CONTAINER_SSL_CERTIFICATE} ${CONTAINER_SSL_PRIVATE_KEY}  > /certs/server.pem
    ln -fs ${CONTAINER_SSL_CERTIFICATE} /certs/server.crt

    x11vnc -storepasswd ${CONTAINER_VNC_PASSWORD} /home/${CONTAINER_USER}/.vnc/passwd
    x11vnc -display ${CONTAINER_DISPLAY} -bg -nopw -xkb -usepw -shared -repeat -loop -forever -ssldir /certs -ssl SAVE &

    wait $NODE_PID
}

export CONTAINER_SCREEN_WIDTH=${CONTAINER_SCREEN_WIDTH:-'1024'}
export CONTAINER_SCREEN_HEIGHT=${CONTAINER_SCREEN_HEIGHT:-'768'}
export CONTAINER_SCREEN_DEPTH=${CONTAINER_SCREEN_DEPTH:-'24'}
export CONTAINER_DISPLAY=${CONTAINER_DISPLAY:-':0'}
export CONTAINER_VNC_PASSWORD=${CONTAINER_VNC_PASSWORD:-'password'}
export CONTAINER_USER=${CONTAINER_USER:-'jalbum'}
export CONTAINER_DATADIR=${CONTAINER_DATADIR:-'/var/www/html'}

export GEOMETRY="$CONTAINER_SCREEN_WIDTH""x""$CONTAINER_SCREEN_HEIGHT""x""$CONTAINER_SCREEN_DEPTH"

export CONTAINER_SSL_CERTIFICATE=${PROJECT_SSL_CERTIFICATE:-'/certs/certificate.pem'}
export CONTAINER_SSL_PRIVATE_KEY=${PROJECT_SSL_PRIVATE_KEY:-'/certs/private-key.pem'}

export DISPLAY=${DISPLAY:-':1'}
export MODE=${MODE:-'local'}

export JALBUM_ALBUM_DIRECTORY=${JALBUM_ALBUM_DIRECTOR:-'.'}
export JALBUM_IMAGES_DIRECTORY=${JALBUM_IMAGES_DIRECTORY:-'./full-size-images'}
export JALBUM_PROJECT_FILE=${JALBUM_PROJECT_FILE:-"$JALBUM_IMAGES_DIRECTORY/jalbum-settings.jap"}
export JALBUM_HOME_ADDRESS=${JALBUM_HOME_ADDRESS:-'https://localhost:8080'}

[ -n "$DEBUG" ] && set -x

echo "MODE: $MODE"
case "$MODE" in
    "headless")
        COMMAND=(
            java
            -Djava.awt.headless=true
            -jar /jAlbum/JAlbum.jar
            -projectFile $JALBUM_PROJECT_FILE
            -homepageAddress $JALBUM_HOME_ADDRESS
            -directory $CONTAINER_DATADIR/$JALBUM_IMAGES_DIRECTORY
            -outputDirectory $CONTAINER_DATADIR/$JALBUM_ALBUM_DIRECTORY
        )
        ${COMMAND[@]}
    ;;
    "passthrough")
        echo "$@"
        "$@"
    ;;
    "remote")
        remote
    ;;
    "local")
        echo $DISPLAY
        /usr/bin/java -Xmx1200M -jar /jAlbum/JAlbum.jar
    ;;
    *)
        echo "Possible modes: headless, passthrough, remote, local"
    ;;
esac
