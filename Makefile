
.DEFAULT_GOAL := help

include $(shell pwd)/.env
#include $(shell pwd)/Makefile.variables
MYSELF := TOP/Makefile
MY_DIR := $(shell pwd)
LOGFILE ?= $(MY_DIR)/build.log

ifdef PROJECT_DOCKER_HOST
DOCKER_HOST = ${PROJECT_DOCKER_HOST}
else
DOCKER_HOST = le-registry.le-domain.le-com
endif

ifdef PROJECT_DOCKER_COMPOSE_SERVICE
SERVICE := $(PROJECT_DOCKER_COMPOSE_SERVICE)
else
SERVICE := defult-to-be-changed
endif

ifdef PROJECT_IMAGE_NAME
IMAGE_NAME=$(DOCKER_HOST)/${PROJECT_IMAGE_NAME}
else
IMAGE_NAME=$(DOCKER_HOST)/le-user/le-project/$(SERVICE)
endif

include $(shell pwd)/common/checks
-include $(shell pwd)/common/Makefile*

help:
	@echo
	@echo Targets:
	@cat Makefile* common/Makefile* | grep '^.PHONY: .* #' | sort | sed 's/\.PHONY: \(.*\) # \(.*\)/\1: \2/'
	@echo
	@echo 'Variables:'
	@echo 	DOCKER_HOST: $(DOCKER_HOST)
	@echo 	IMAGE_NAME: $(IMAGE_NAME)
	@echo 	IMAGE_VERSION: $(IMAGE_VERSION)
	@echo 	SERVICE: $(SERVICE)
	@echo 	LOGFILE: $(LOGFILE)
	@echo

show-vars: show_vars
show_vars: show-checks
	@echo DOCKER_HOST: $(DOCKER_HOST)
	@echo IMAGE_NAME: $(IMAGE_NAME)
	@echo IMAGE_VERSION: $(IMAGE_VERSION)
	@echo SERVICE: $(SERVICE)
	@echo LOGFILE: $(LOGFILE)
	@echo
.PHONY: show-vars # show variables as Makefile sees them

check: check-docker-compose check-docker check-sudo
	@echo $(shell date) $(MYSELF): success: $@ target
.PHONY: check # check for binaries needed

all: image
	@echo $(shell date) $(MYSELF): success: $@ target
.PHONY: all # builds, tags and pushes image from docker-compose.yml
