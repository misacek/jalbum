FROM openjdk:8

ARG BUILD_DATE
ARG BUILD_VERSION
ARG DEBUG=1

ARG CONTAINER_USER_UID=5001
ARG CONTAINER_USER=jalbum

ARG CONTAINER_DATADIR=/var/www/html/

ARG CONTAINER_CERTS=/certs
#ARG CONTAINER_PACKAGES="x11vnc xvfb bsdutils default-jre openjfx fluxbox xvt xauth"
ARG CONTAINER_PACKAGES="x11vnc xvfb bsdutils default-jre openjfx fluxbox xvt"
ARG CONTAINER_BUILD_PACKAGES="curl unzip"
ARG JALBUM_ZIP="src/jalbum-14.1.zip"

LABEL maintainer="Michal Novacek <michal.novacek@gmail.com>"
LABEL description="\
Dockerfile used to build this image is here: \
https://gitlab.com/misacek/docker-images/blob/jalbum/Dockerfile \
"
LABEL build_version="$BUILD_VERSION"
LABEL build_date="$BUILD_DATE"

RUN \
    apt-get update && apt-get -y upgrade && \
    apt-get -y install \
        --no-install-recommends \
        $CONTAINER_PACKAGES ${CONTAINER_BUILD_PACKAGES}

RUN \
    adduser \
        --disabled-password \
        --uid ${CONTAINER_USER_UID} \
        --gid 0 \
        --home /home/${CONTAINER_USER} \
        --gecos 'el jalbum user' \
        ${CONTAINER_USER} && \
    mkdir -p \
        /home/${CONTAINER_USER}/.vnc \
        ${CONTAINER_DATADIR} \
        ${CONTAINER_CERTS} \
        /entrypoint.d && \
    touch ${CONTAINER_CERTS}/ssl-certificate.pem ${CONTAINER_CERTS}/ssl-private-key.pem

# download und unpack jalbum
COPY $JALBUM_ZIP src/skins/ExhibitPlus.zip /tmp/
RUN \
    unzip /tmp/$(basename $JALBUM_ZIP) && \
    cd /jAlbum/skins && unzip /tmp/ExhibitPlus.zip

COPY 01.run.sh /entrypoint.d/
COPY entrypoint.sh /entrypoint.sh

# cleanup
RUN \
    chown -R ${CONTAINER_USER}:root \
        /home/${CONTAINER_USER} \
        ${CONTAINER_DATADIR} \
        ${CONTAINER_CERTS} \
        /entrypoint.d /entrypoint.sh && \
    chmod -R 755 /entrypoint.d /entrypoint.sh && \
    apt-get -y remove ${CONTAINER_BUILD_PACKAGES} && \
    apt-get clean

WORKDIR $CONTAINER_DATADIR

EXPOSE 5901

USER ${CONTAINER_USER_UID}

VOLUME ["${CONTAINER_DATADIR}", "${CONTAINER_CERTS}"]

ENTRYPOINT ["/entrypoint.sh"]
